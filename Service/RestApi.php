<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace Sorin\Blogpost\Service;

use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;
use Magento\Framework\Webapi\Rest\Request;
use Psr\Log\LoggerInterface;
use Sorin\Blogpost\Exception\InvalidAPIResponse;
use Sorin\Blogpost\Model\CompositeConfigProvider;

class RestApi
{
    const HTTP_STATUS_OK = 200;
    const HTTP_STATUS_CREATED = 201;
    const HTTP_STATUS_NO_BODY_DELETED = 204;

    /**
     * @var ResponseFactory
     */
    private ResponseFactory $responseFactory;

    /**
     * @var ClientFactory
     */
    private ClientFactory $clientFactory;

    /**
     * @var CompositeConfigProvider
     */
    private CompositeConfigProvider $configProvider;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * RestApi constructor.
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param CompositeConfigProvider $configProvider
     * @param LoggerInterface $logger
     */
    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        CompositeConfigProvider $configProvider,
        LoggerInterface $logger
    ) {
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
        $this->configProvider = $configProvider;
        $this->logger = $logger;
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @param string $requestMethod
     * @return array|null
     * @throws InvalidAPIResponse
     */
    public function execute(string $endpoint, $params = [], string $requestMethod = Request::HTTP_METHOD_GET): ?array
    {
        $config = $this->configProvider->getConfig();

        if (empty($config['api_endpoint']) && empty($config['token'])) {
            $this->logger->error(__CLASS__ . ' >>> No configuration provided for blog posts API');
            throw new InvalidAPIResponse('No configuration provided for blog posts API');
        }

        $params['headers'] = [ 'Authorization' => 'Bearer ' . $config['token']];

        $response = $this->doRequest(
            $config['api_endpoint'] . DIRECTORY_SEPARATOR . $endpoint,
            $params,
            $requestMethod
        );

        if (!in_array($response->getStatusCode(), [
            static::HTTP_STATUS_OK,
            static::HTTP_STATUS_CREATED,
            static::HTTP_STATUS_NO_BODY_DELETED
        ])) {
            $this->logger->error(
                __CLASS__ . ' >>> Status code: ' . $response->getStatusCode() . ' | Response: ' . json_encode($response)
            );

            throw new InvalidAPIResponse('API response could not be retrieved');
        }

        $responseBody = $response->getBody();
        return json_decode($responseBody->getContents(), true);
    }

    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = Request::HTTP_METHOD_GET
    ): Response {
        /** @var Client $client */
        $client = $this->clientFactory->create();

        try {
            $response = $client->request(
                $requestMethod,
                $uriEndpoint,
                $params
            );
        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);
        }

        return $response;
    }
}
