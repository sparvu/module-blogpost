<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Sorin\Blogpost\Api\Data\PostInterface;

interface PostRepositoryInterface
{
    /**
     * @param int|null $entityId
     * @return \Sorin\Blogpost\Api\Data\PostInterface|null
     */
    public function getByEntityId(?int $entityId): ?PostInterface;

    /**
     * @param \Sorin\Blogpost\Api\Data\PostInterface $post
     * @return \Sorin\Blogpost\Api\Data\PostInterface
     */
    public function save(PostInterface $post): PostInterface;

    /**
     * @param \Sorin\Blogpost\Api\Data\PostInterface $post
     * @return void
     */
    public function delete(PostInterface $post);

    /**
     * @param $entityId
     * @return void
     */
    public function deleteById($entityId): void;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Sorin\Blogpost\Api\Data\PostSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): Data\PostSearchResultsInterface;
}
