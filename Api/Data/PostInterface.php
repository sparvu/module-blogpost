<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Sorin\Blogpost\Api\Data;

interface PostInterface
{
    const ENTITY_ID = 'id';
    const TITLE = 'title';
    const CONTENT = 'content';

    /**
     * Get EntityId.
     *
     * @return int|null
     */
    public function getEntityId(): ?int;

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId);

    /**
     * Get Name.
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Set Name.
     */
    public function setTitle($title);

    /**
     * Get SubName.
     *
     * @return string
     */
    public function getContent(): string;

    /**
     * Set SubName.
     */
    public function setContent($content);
}
