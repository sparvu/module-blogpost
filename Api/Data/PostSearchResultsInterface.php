<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface PostSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Sorin\Blogpost\Api\Data\PostInterface[]
     */
    public function getItems();

    /**
     * @param \Sorin\Blogpost\Api\Data\PostInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
