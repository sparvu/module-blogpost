/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */

var config = {
    map: {
        '*': {
            blogpost: 'Sorin_Blogpost/js/action/post-add',
            blogpostcomment: 'Sorin_Blogpost/js/action/comment-add'
        }
    }
};
