/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */

define([
    'jquery',
    'Sorin_Blogpost/js/view/posts',
    'jquery-ui-modules/widget',
], function ($, posts) {
    'use strict';


    var config = window.postsConfig,
        data = {
            title: "",
            body: ""
        };

    $.widget('mage.blogpost', {

        _create: function () {
            this.title = $(this.options.titleSelector);
            this.content = $(this.options.contentSelector);
            this.form = $(this.options.formSelector);

            $(this.options.addButton).on('click', $.proxy(function () {

                data.title = this.title.val();
                data.body = this.content.val();

                var valid = this.form.validation('isValid');

                if (!config.api_endpoint || !config.token) {
                    console.log('BLOGPOSTS: Please check API admin configurations');
                }

                if (valid) {
                    jQuery.ajax({
                        showLoader: true,
                        url: config.api_endpoint + '/users/' + this.getRandomInt(1, 1000) + '/posts',
                        type: "POST",
                        data: data,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', 'Bearer ' + config.token);
                        }
                    }).done(function () {
                        jQuery('#block-post-add').collapsible("deactivate");
                        jQuery('#blog-post-form').trigger('reset');
                        posts.call();
                    });
                }

            }, this));
        },
        getRandomInt: function (min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    });

    return $.mage.blogpost;
});
