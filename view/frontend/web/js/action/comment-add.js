/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */

define([
    'jquery',
    'Sorin_Blogpost/js/view/comments',
    'jquery-ui-modules/widget',
], function ($, posts) {
    'use strict';


    var config = window.commentsConfig,
        currentPostId = window.currentPostId,
        data = {
            name: "",
            email: "",
            body: ""
        };

    $.widget('mage.blogpostcomment', {

        _create: function () {
            this.name = $(this.options.nameSelector);
            this.email = $(this.options.emailSelector);
            this.body = $(this.options.commentSelector);
            this.form = $(this.options.formSelector);

            $(this.options.addButton).on('click', $.proxy(function () {

                data.name = this.name.val();
                data.email = this.email.val();
                data.body = this.body.val();

                var valid = this.form.validation('isValid');

                if (!config.api_endpoint || !config.token) {
                    console.log('BLOGPOSTS: Please check API admin configurations');
                }

                if (valid) {
                    jQuery.ajax({
                        showLoader: true,
                        url: config.api_endpoint + '/posts/' + currentPostId + '/comments',
                        type: "POST",
                        data: data,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', 'Bearer ' + config.token);
                        }
                    }).done(function () {
                        jQuery('#block-comment-add').collapsible("deactivate");
                        jQuery('#blog-comment-form').trigger('reset');
                        posts.call();
                    });
                }

            }, this));
        }
    });

    return $.mage.blogpostcomment;
});
