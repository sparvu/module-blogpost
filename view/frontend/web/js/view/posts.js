/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
define([
    'ko',
    'uiComponent',
    'mage/storage',
    'mage/url'
], function (ko, Component, storage, url) {
    'use strict';

    var self,
        config = window.postsConfig;

    return Component.extend({

        posts: ko.observable({}),

        initialize: function () {
            this._super();
            self = this;

            self.getApiPosts();
        },
        getApiPosts: function (){
            jQuery.ajax({
                showLoader: true,
                url: config.api_endpoint + '/posts',
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + config.token);
                }
            }).done(function (data) {
                self.posts(data);
            });
        },
        getPosts: function() {
            return self.posts;
        }
    });
});
