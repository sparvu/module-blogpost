/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
define([
    'ko',
    'uiComponent',
    'mage/storage',
    'mage/url'
], function (ko, Component, storage, url) {
    'use strict';

    var self,
        config = window.commentsConfig,
        currentPostId = window.currentPostId;

    return Component.extend({

        comments: ko.observable({}),

        initialize: function () {
            this._super();
            self = this;

            self.getApiComments();
        },
        getApiComments: function (){
            jQuery.ajax({
                showLoader: true,
                url: config.api_endpoint + '/posts/' + currentPostId + '/comments',
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + config.token);
                }
            }).done(function (data) {
                self.comments(data);
            });
        },
        getComments: function() {
            return self.comments;
        }
    });
});
