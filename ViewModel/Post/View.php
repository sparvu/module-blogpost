<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\ViewModel\Post;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Sorin\Blogpost\Api\Data\PostInterface;
use Sorin\Blogpost\Api\PostRepositoryInterface;
use Sorin\Blogpost\Model\Registry;

class View implements ArgumentInterface
{
    /**
     * @var PostRepositoryInterface
     */
    private PostRepositoryInterface $postRepository;

    /**
     * @var Registry
     */
    private Registry $registry;

    /**
     * View constructor.
     * @param PostRepositoryInterface $postRepository
     * @param Registry $registry
     */
    public function __construct(
        PostRepositoryInterface $postRepository,
        Registry $registry
    ) {
        $this->postRepository = $postRepository;
        $this->registry   = $registry;
    }

    /**
     * @return int
     */
    public function getCurrentPostId()
    {
        return $this->registry->getPostId();
    }

    /**
     * @return PostInterface|null
     */
    public function getPost(): ?PostInterface
    {
        try {
            $post = $this->postRepository->getByEntityId($this->getCurrentPostId());
        } catch (NoSuchEntityException $e) {
            // TODO log more errors
            return null;
        }

        return $post;
    }
}
