<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Sorin\Blogpost\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Sorin\Blogpost\Model\CompositeConfigProvider;
use Magento\Framework\Serialize\SerializerInterface;

class Config implements ArgumentInterface
{
    /**
     * @var CompositeConfigProvider
     */
    private CompositeConfigProvider $compositeConfigProvider;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $encoder;

    /**
     * View constructor.
     * @param CompositeConfigProvider $compositeConfigProvider
     * @param SerializerInterface $encoder
     */
    public function __construct(
        CompositeConfigProvider $compositeConfigProvider,
        SerializerInterface $encoder
    ) {
        $this->compositeConfigProvider = $compositeConfigProvider;
        $this->encoder   = $encoder;
    }

    /**
     * @return bool|string
     */
    public function getBlogConfigJson()
    {
        return $this->encoder->serialize($this->compositeConfigProvider->getConfig());
    }
}
