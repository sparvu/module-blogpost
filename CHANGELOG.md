Sorin Blogpost
All notable changes to this module will be documented in this file.

Added | Changed | Fixed | Removed

[1.0.0] - 2022-02-19

# Admin:
- Added: Admin configuration for I/O and API settings
- Added: Content / Blog / Post section for admins to manage posts (create, update, delete)

# Storefront
- Added: Users can post comments on their favourite posts (visible by all users)
- Added: Users can post new topics without having to register an account (anonymously)


Upcoming features:
- Post topics/comments only for registered users
- Create custom logging for better monitoring
- Adding admin filters on the grid listing



