<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Exception;

use Exception;

class InvalidAPIResponse extends Exception
{
}
