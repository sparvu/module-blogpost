<?php
namespace Sorin\Blogpost\Block;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Sorin\Blogpost\Model\CompositeConfigProvider;
use Sorin\Blogpost\Service\RestApi;
use Magento\Framework\Serialize\SerializerInterface;

class Posts extends Template
{
    /**
     * @var RestApi
     */
    private $apiService;

    /**
     * @var CompositeConfigProvider
     */
    private $compositeConfigProvider;

    /**
     * @var SerializerInterface
     */
    private $encoder;

    /**
     * @var array|LayoutProcessorInterface[]
     */
    private $layoutProcessors;

    /**
     * Posts constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context,
        CompositeConfigProvider $compositeConfigProvider,
        RestApi $apiService,
        SerializerInterface $encoder,
        array $layoutProcessors = [],
        array $data = []
    ) {
        parent::__construct($context);

        $this->compositeConfigProvider = $compositeConfigProvider;
        $this->apiService = $apiService;
        $this->encoder = $encoder;
        $this->layoutProcessors = $layoutProcessors;
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
    }

    /**
     * @return array
     */
    public function getPosts()
    {
        return $this->apiService->execute();
    }

    public function getJsLayout()
    {
        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
        return $this->encoder->serialize($this->jsLayout);
    }

    /**
     * @param $post
     * @return string
     */
    public function getPostPreview($post): string
    {
        if (strlen($post) > 250) {
            $preview = substr($post, 0, 250) . ' ...';
        } else {
            $preview = $post;
        }

        return $preview;
    }

    /**
     * @return bool|string
     */
    public function getPostsConfigJson()
    {
        return $this->encoder->serialize($this->compositeConfigProvider->getConfig());
    }
}
