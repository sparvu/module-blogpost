<?php
namespace Sorin\Blogpost\Block\Post;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Serialize\SerializerInterface;

abstract class AbstractPost extends Template
{
    /**
     * @var SerializerInterface
     */
    private $encoder;

    /**
     * @var array|LayoutProcessorInterface[]
     */
    private $layoutProcessors;

    /**
     * PostList constructor.
     * @param Context $context
     * @param SerializerInterface $encoder
     * @param array $layoutProcessors
     * @param array $data
     */
    public function __construct(
        Context $context,
        SerializerInterface $encoder,
        array $layoutProcessors = [],
        array $data = []
    ) {
        parent::__construct($context);

        $this->encoder = $encoder;
        $this->layoutProcessors = $layoutProcessors;
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
    }

    /**
     * @return bool|string
     */
    public function getJsLayout()
    {
        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
        return $this->encoder->serialize($this->jsLayout);
    }
}
