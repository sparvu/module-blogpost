<?php

namespace Sorin\Blogpost\Test\Unit\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\StreamInterface;
use Sorin\Blogpost\Service\RestApi;
use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;
use Psr\Log\LoggerInterface;
use Sorin\Blogpost\Exception\InvalidAPIResponse;
use Sorin\Blogpost\Model\CompositeConfigProvider;

class RestApiTest extends TestCase
{
    /**
     * @var MockObject | Client
     */
    private $clientMock;

    /**
     * @var MockObject | StreamInterface
     */
    private $streamMock;

    /**
     * @var MockObject | Response
     */
    private $responseMock;

    /**
     * @var MockObject | LoggerInterface
     */
    private $loggerMock;

    /**
     * @var MockObject | CompositeConfigProvider
     */
    private $configProviderMock;

    /**
     * @var MockObject | ResponseFactory
     */
    private $responseFactoryMock;

    /**
     * @var MockObject | ClientFactory
     */
    private $clientFactoryMock;

    /**
     * @var RestApi
     */
    private RestApi $service;

    /**
     * test init
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->clientFactoryMock = $this->getMockBuilder(ClientFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->responseFactoryMock = $this->getMockBuilder(ResponseFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->configProviderMock = $this->createMock(CompositeConfigProvider::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);
        $this->responseMock = $this->createMock(Response::class);
        $this->streamMock = $this->createMock(StreamInterface::class);
        $this->clientMock = $this->createMock(Client::class);

        $this->service = new RestApi(
            $this->clientFactoryMock,
            $this->responseFactoryMock,
            $this->configProviderMock,
            $this->loggerMock
        );
    }

    /**
     * @param $config
     * @param $statusCode
     * @param $params
     * @param $method
     * @param $json
     * @param $expected
     *
     * @dataProvider dataProvider
     * @throws InvalidAPIResponse
     */
    public function testExecute($config, $apiUrl, $statusCode, $params, $method, $json, $expected)
    {
        $this->configProviderMock->expects($this->once())
            ->method('getConfig')
            ->willReturn($config);

        $this->clientFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->clientMock);

        $this->clientMock->expects($this->once())
            ->method('request')
            ->willReturn($this->responseMock);

        $this->responseMock->expects($this->once())
            ->method('getStatusCode')
            ->willReturn($statusCode);

        $this->responseMock->expects($this->once())
            ->method('getBody')
            ->willReturn($this->streamMock);

        $this->streamMock->expects($this->once())
            ->method('getContents')
            ->willReturn($json);

        $this->assertEquals($expected, $this->service->execute($apiUrl, $params, $method));
    }

    /**
     * @return array[]
     */
    public function dataProvider(): array
    {
        return [
            // get all posts
            [
                [ // config
                    'enable' => true,
                    'api_endpoint' => 'https://test.com',
                    'token' => '32u4hg2fgierherio23423',
                    'storeCode' => 'default'
                ],
                // apiUrl
                'posts/',
                // statusCode,
                200,
                // params
                [],
                // HTTP Method
                'GET',
                '{"title": "This is a title", "body": "this is the post content"}', // json response
                ['title' => 'This is a title', 'body' => 'this is the post content'] // json decoded
            ],
            // get post by ID
            [
                [ // config
                    'enable' => true,
                    'api_endpoint' => 'https://test.com',
                    'token' => '32u4hg2fgierherio23423',
                    'storeCode' => 'default'
                ],
                // apiUrl
                'posts/1234',
                // statusCode
                200,
                // params
                [],
                // HTTP Method
                'GET',
                // json response
                '{"id": 1234, "user_id": 1222, "title": "This is a title", "body": "this is the post content"}',
                // json decoded
                ['id' => 1234, 'user_id' => 1222, 'title' => 'This is a title', 'body' => 'this is the post content']
            ],
            // get post comments
            [
                [ // config
                    'enable' => true,
                    'api_endpoint' => 'https://test.com',
                    'token' => '32u4hg2fgierherio23423',
                    'storeCode' => 'default'
                ],
                // apiUrl
                'posts/1234/comments',
                // statusCode
                200,
                // params
                [],
                // HTTP Method
                'GET',
                // json response
                '{"id": 1686,"post_id": 1678,"name": "Name","email": "e@e.ee","body": "text"}',
                // json decoded
                ['id' => 1686, 'post_id' => 1678, 'name' => 'Name', 'email' => 'e@e.ee', 'body' => 'text']
            ],
            // add new post
            [
                [ // config
                    'enable' => true,
                    'api_endpoint' => 'https://test.com',
                    'token' => '32u4hg2fgierherio23423',
                    'storeCode' => 'default'
                ],
                // apiUrl
                'posts',
                // statusCode
                201,
                // params
                ['form_params'=>['title' => 'Title', 'body' => 'content text', 'user_id' => 2961]],
                // HTTP Method
                'POST',
                // json response
                '{"id": 1739,"user_id": 2961,"title": "Title","body": "content text"}',
                // json decoded
                ['id' => 1739, 'user_id' => 2961, 'title' => 'Title', 'body' => 'content text']
            ],
            // delete post
            [
                [ // config
                    'enable' => true,
                    'api_endpoint' => 'https://test.com',
                    'token' => '32u4hg2fgierherio23423',
                    'storeCode' => 'default'
                ],
                // apiUrl
                'posts/1234',
                // statusCode
                204,
                // params
                [],
                // HTTP Method
                'DELETE',
                // json response
                '',
                // json decoded
                ''
            ],
            // update post
            [
                [ // config
                    'enable' => true,
                    'api_endpoint' => 'https://test.com',
                    'token' => '32u4hg2fgierherio23423',
                    'storeCode' => 'default'
                ],
                // apiUrl
                'posts/1234',
                // statusCode
                200,
                // params
                ['form_params'=>['title' => 'Title', 'body' => 'content text', 'id' => 2961]],
                // HTTP Method
                'PATCH',
                // json response
                '{"id": 1739,"user_id": 2961,"title": "Title","body": "content text"}',
                // json decoded
                ['id' => 1739, 'user_id' => 2961, 'title' => 'Title', 'body' => 'content text']
            ],
            // add new comment
            [
                [ // config
                    'enable' => true,
                    'api_endpoint' => 'https://test.com',
                    'token' => '32u4hg2fgierherio23423',
                    'storeCode' => 'default'
                ],
                // apiUrl
                'posts/1234/comments',
                // statusCode
                201,
                // params
                ['form_params'=>['name' => 'John', 'email' => 'john@aa.com', 'body' => 'comment text']],
                // HTTP Method
                'POST',
                // json response
                '{"id": 1709,"post_id": 1234,"name": "John","email": "john@aa.com","body": "comment text"}',
                // json decoded
                ['id' => 1709, 'post_id' => 1234, 'name' => 'John', 'email' => 'john@aa.com', 'body' => 'comment text']
            ]
        ];
    }
}
