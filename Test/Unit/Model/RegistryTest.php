<?php

namespace Sorin\Blogpost\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Sorin\Blogpost\Model\Registry;

class RegistryTest extends TestCase
{
    /**
     * @var Registry
     */
    private $model;

    /**
     * test init
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Registry();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testGetPostId($id)
    {
        if ($id) {
            $this->model->setPostId($id);
        }

        $this->assertEquals($id, $this->model->getPostId());
    }

    public function dataProvider()
    {
        return [
            [10],
            [null]
        ];
    }
}
