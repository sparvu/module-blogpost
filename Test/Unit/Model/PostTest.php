<?php

namespace Sorin\Blogpost\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Sorin\Blogpost\Model\Post;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class PostTest extends TestCase
{
    /**
     * @var Post
     */
    private Post $model;

    /**
     * test init
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Post(
            $this->createMock(Context::class),
            $this->createMock(Registry::class)
        );
    }

    public function testGetEntityId()
    {
        $id = 10;
        $this->model->setEntityId($id);
        $this->assertEquals($id, $this->model->getEntityId());
    }

    public function testGetTitle()
    {
        $title = 'Title';
        $this->model->setTitle($title);
        $this->assertEquals($title, $this->model->getTitle());
    }

    public function testGetContent()
    {
        $content = 'content text';
        $this->model->setContent($content);
        $this->assertEquals($content, $this->model->getContent());
    }

    public function testGetIdentities()
    {
        $cacheTag = 'blog_post_';
        $this->model->setEntityId(100);

        $expected = [$cacheTag . $this->model->getEntityId()];
        $this->assertEquals($expected, $this->model->getIdentities());
    }
}
