<?php

namespace Sorin\Blogpost\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Sorin\Blogpost\Model\PostsConfigProvider;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;

class PostsConfigProviderTest extends TestCase
{
    /**
     * @var MockObject | Context
     */
    private $contextMock;

    /**
     * @var MockObject | ScopeConfigInterface
     */
    private $scopeConfigMock;

    /**
     * @var PostsConfigProvider
     */
    private PostsConfigProvider $configProvider;

    protected function setUp(): void
    {
        parent::setUp();

        $this->contextMock = $this->createMock(Context::class);
        $this->scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $this->contextMock->expects($this->once())
            ->method('getScopeConfig')
            ->willReturn($this->scopeConfigMock);

        $this->configProvider = new PostsConfigProvider($this->contextMock);
    }

    /**
     * @param $isEnabled
     * @param $apiEndpoint
     * @param $apiToken
     * @param $expected
     *
     * @dataProvider dataProvider
     */
    public function testGetConfig($isEnabled, $apiEndpoint, $apiToken, $expected)
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('isSetFlag')
            ->willReturn($isEnabled);

        $this->scopeConfigMock->expects($this->at(1))
            ->method('getValue')
            ->willReturn($apiEndpoint);

        $this->scopeConfigMock->expects($this->at(2))
            ->method('getValue')
            ->willReturn($apiToken);

        $this->assertEquals($expected, $this->configProvider->getConfig());
    }

    /**
     * @return array[]
     */
    public function dataProvider(): array
    {
        return [
            [
                true, 'https://test.com', '32u4hg2fgierherio23423',
                [
                    'enable' => true,
                    'api_endpoint' => 'https://test.com',
                    'token' => '32u4hg2fgierherio23423',
                    'storeCode' => 'default'
                ]
            ],
            [
                false, null, null,
                [
                    'enable' => false,
                    'api_endpoint' => null,
                    'token' => null,
                    'storeCode' => 'default'
                ]
            ]
        ];
    }
}
