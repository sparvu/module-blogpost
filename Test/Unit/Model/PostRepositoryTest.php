<?php

namespace Sorin\Blogpost\Test\Unit\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Sorin\Blogpost\Exception\InvalidAPIResponse;
use Sorin\Blogpost\Model\Post;
use Sorin\Blogpost\Model\PostFactory;
use Sorin\Blogpost\Api\Data\PostSearchResultsInterfaceFactory;
use Magento\Framework\Data\CollectionFactory;
use Sorin\Blogpost\Model\PostRepository;
use Sorin\Blogpost\Service\RestApi;

class PostRepositoryTest extends TestCase
{
    /**
     * @var MockObject | InvalidAPIResponse
     */
    private $exceptionMock;

    /**
     * @var MockObject | Post
     */
    private $postMock;

    /**
     * @var MockObject | PostSearchResultsInterfaceFactory
     */
    private $postSearchResultsFactoryMock;

    /**
     * @var MockObject | CollectionFactory
     */
    private $collectionMock;

    /**
     * @var MockObject | PostFactory
     */
    private $postFactoryMock;

    /**
     * @var MockObject | RestApi
     */
    private $restApiServiceMock;

    /**
     * @var PostRepository
     */
    private PostRepository $model;

    /**
     * test init
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->restApiServiceMock = $this->createMock(RestApi::class);
        $this->postFactoryMock = $this->getMockBuilder(PostFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->postSearchResultsFactoryMock = $this->getMockBuilder(PostSearchResultsInterfaceFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->collectionFactoryMock = $this->getMockBuilder(CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->postMock = $this->createMock(Post::class);
        $this->exceptionMock = $this->createMock(InvalidAPIResponse::class);

        $this->model = new PostRepository(
            $this->restApiServiceMock,
            $this->postFactoryMock,
            $this->collectionFactoryMock,
            $this->postSearchResultsFactoryMock
        );
    }

    /**
     * @param $apiResponse
     * @param $entityId
     * @param $exception
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @dataProvider dataProvider
     */
    public function testGetByEntityId($apiResponse, $entityId, $exception)
    {
        $this->restApiServiceMock->expects($this->once())
            ->method('execute')
            ->willReturn($apiResponse);

        $this->postFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->postMock);

        $this->postMock->expects($this->once())
            ->method('setEntityId')
            ->with($entityId)
            ->willReturn($this->postMock);

        $this->postMock->expects($this->once())
            ->method('setTitle')
            ->with($apiResponse['title'])
            ->willReturn($this->postMock);

        $this->postMock->expects($this->once())
            ->method('setContent')
            ->with($apiResponse['body'])
            ->willReturn($this->postMock);

        $this->assertEquals($this->postMock, $this->model->getByEntityId($entityId));
    }

    public function dataProvider()
    {
        return [
            [
                ['title' => 'title', 'body' => 'content text'], // apiResponse
                123, // entityId
                false // exception
            ],
            [
                ['title' => 'title', 'body' => 'content text'], // apiResponse
                123, // entityId
                true // exception
            ]
        ];
    }
}
