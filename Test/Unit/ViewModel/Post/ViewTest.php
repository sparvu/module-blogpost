<?php

namespace Sorin\Blogpost\Test\Unit\ViewModel\Post;

use Magento\Framework\Exception\NoSuchEntityException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Sorin\Blogpost\Api\Data\PostInterface;
use Sorin\Blogpost\Api\PostRepositoryInterface;
use Sorin\Blogpost\Exception\InvalidAPIResponse;
use Sorin\Blogpost\Model\Registry;
use Sorin\Blogpost\ViewModel\Post\View;

class ViewTest extends TestCase
{
    /**
     * @var MockObject | PostRepositoryInterface
     */
    private $postRepositoryMock;

    /**
     * @var MockObject | Registry
     */
    private $registryMock;

    /**
     * @var MockObject | PostInterface
     */
    private $postMock;

    /**
     * @var MockObject | NoSuchEntityException
     */
    private $exceptionMock;

    /**
     * @var View
     */
    private View $viewModel;

    /**
     * test init
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->postRepositoryMock = $this->createMock(PostRepositoryInterface::class);
        $this->registryMock = $this->createMock(Registry::class);
        $this->postMock = $this->createMock(PostInterface::class);
        $this->exceptionMock = $this->createMock(NoSuchEntityException::class);
//        $this->exceptionMock = $this->createMock(InvalidAPIResponse::class);

        $this->viewModel = new View(
            $this->postRepositoryMock,
            $this->registryMock
        );
    }

    /**
     * @param $postId
     * @param $exception
     *
     * @dataProvider dataProviderGetPost
     */
    public function testGetPost($postId, $exception)
    {
        $this->registryMock->expects($this->once())
            ->method('getPostId')
            ->willReturn($postId);

        if ($exception) {
            $this->postRepositoryMock->expects($this->once())
                ->method('getByEntityId')
                ->with($postId)
                ->willThrowException($this->exceptionMock);

            $this->assertNull($this->viewModel->getPost());
        } else {
            $this->postRepositoryMock->expects($this->once())
                ->method('getByEntityId')
                ->with($postId)
                ->willReturn($this->postMock);

            $this->assertEquals($this->postMock, $this->viewModel->getPost());
        }
    }

    /**
     * @return array[]
     */
    public function dataProviderGetPost(): array
    {
        return [
            [1000, false],
            [2000, true]
        ];
    }
}
