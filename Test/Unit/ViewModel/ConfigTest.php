<?php

namespace Sorin\Blogpost\Test\Unit\ViewModel;

use Magento\Framework\Serialize\SerializerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Sorin\Blogpost\Model\CompositeConfigProvider;
use Sorin\Blogpost\ViewModel\Config;

class ConfigTest extends TestCase
{
    /**
     * @var MockObject | CompositeConfigProvider
     */
    private $compositeConfigProviderMock;

    /**
     * @var MockObject | SerializerInterface
     */
    private $encoderMock;

    /**
     * @var Config
     */
    private $viewModel;

    protected function setUp(): void
    {
        parent::setUp();

        $this->compositeConfigProviderMock = $this->createMock(CompositeConfigProvider::class);
        $this->encoderMock = $this->createMock(SerializerInterface::class);

        $this->viewModel = new Config(
            $this->compositeConfigProviderMock,
            $this->encoderMock
        );
    }

    /**
     * @dataProvider dataProvider
     *
     * @param $configData
     */
    public function testGetBlogConfigJson($configData, $expectedResult)
    {
        $this->compositeConfigProviderMock->expects($this->once())
            ->method('getConfig')
            ->willReturn($configData);

        $this->encoderMock->expects($this->once())
            ->method('serialize')
            ->with($configData)
            ->willReturn($expectedResult);

        $this->assertEquals($expectedResult, $this->viewModel->getBlogConfigJson());
    }

    /**
     * @return array[]
     */
    public function dataProvider()
    {
        return [
            [
                ['enable' => true, 'api_endpoint' => 'https://api.endpoint.com/'],
                '{"enable": "true", "api_endpoint": "https://api.endpoint.com/"}'
            ]
        ];
    }
}
