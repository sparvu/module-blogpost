<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Ui\Component\Listing\Post\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class Action extends Column
{
    const ROW_EDIT_URL = 'blog/posts/edit';
    const ROW_EDIT_DELETE = 'blog/posts/delete';

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {

                $entityId = $item['id'];
                $item[$this->getData('name')] = [
                    'edit' => [
                        'href' => $this->context->getUrl(
                            static::ROW_EDIT_URL,
                            ['id' => $entityId]
                        ),
                        'label' => __('Edit')
                    ],
                    'remove' => [
                        'href' => $this->context->getUrl(
                            static::ROW_EDIT_DELETE,
                            ['id' => $entityId]
                        ),
                        'label' => __('Remove'),
                        'confirm' => [
                            'title' => __('Delete post'),
                            'message' => __('Are you sure?')
                        ]
                    ]
                ];
            }
        }

        return $dataSource;
    }
}
