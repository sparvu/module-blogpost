<?php

namespace Sorin\Blogpost\Ui\DataProvider\Posts;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Sorin\Blogpost\Api\PostRepositoryInterface;

class DataProvider extends AbstractDataProvider
{
    /**
     * @var PostRepositoryInterface
     */
    private PostRepositoryInterface $postRepository;

    /**
     * DataProvider constructor.
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param PostRepositoryInterface $postRepository
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        PostRepositoryInterface $postRepository,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->postRepository = $postRepository;
        $this->collection = $postRepository->getCollection();
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->collection->toArray();
    }
}
