<?php

namespace Sorin\Blogpost\Ui\DataProvider;

use Magento\Framework\Api\Filter;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Sorin\Blogpost\Api\PostRepositoryInterface;
use Sorin\Blogpost\Model\Registry;

class DataProvider extends AbstractDataProvider
{
    /**
     * @var Registry
     */
    private Registry $registry;

    /**
     * @var PostRepositoryInterface
     */
    private PostRepositoryInterface $postRepository;

    /**
     * DataProvider constructor.
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param Registry $registry
     * @param PostRepositoryInterface $postRepository
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        Registry $registry,
        PostRepositoryInterface $postRepository,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );

        $this->registry = $registry;
        $this->postRepository = $postRepository;
    }

    /**
     * @return array|array[]
     * @throws NoSuchEntityException
     */
    public function getData(): array
    {
        if ($this->registry->getPostId()) {
            $post = $this->postRepository->getByEntityId($this->registry->getPostId());

            $array[$post->getEntityId()] = [
                'id'   => $post->getEntityId(),
                'title' => $post->getTitle(),
                'content' => $post->getContent()
            ];

            return $array;
        }
        return [];
    }

    /**
     * @param Filter $filter
     * @return null
     */
    public function addFilter(Filter $filter)
    {
        return null;
    }
}
