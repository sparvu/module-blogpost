<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Controller\Posts;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\ResultFactory;
use Sorin\Blogpost\Model\CompositeConfigProvider;
use Sorin\Blogpost\Model\Registry;

class View extends Action implements HttpGetActionInterface
{
    /**
     * @var CompositeConfigProvider
     */
    private CompositeConfigProvider $config;

    /**
     * @var Registry
     */
    private Registry $registry;

    /**
     * View constructor.
     * @param Context $context
     * @param CompositeConfigProvider $config
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        CompositeConfigProvider $config,
        Registry $registry
    ) {
        parent::__construct($context);

        $this->config   = $config;
        $this->registry = $registry;
    }

    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        if (!$this->config->getConfig()['enable']) {
            return $this->_redirect('noroute');
        }

        $postId = $this->getRequest()->getParam('id');

        if (!empty($postId)) {
            $this->registry->setPostId($postId);
        }

        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
