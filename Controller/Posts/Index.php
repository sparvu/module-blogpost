<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Controller\Posts;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\ResultFactory;
use Sorin\Blogpost\Model\CompositeConfigProvider;
use Magento\Framework\App\Action\Context;

class Index extends Action implements HttpGetActionInterface
{
    /**
     * @var CompositeConfigProvider
     */
    private CompositeConfigProvider $config;

    /**
     * Index constructor.
     * @param Context $context
     * @param CompositeConfigProvider $config
     */
    public function __construct(
        Context $context,
        CompositeConfigProvider $config
    ) {
        parent::__construct($context);
        $this->config = $config;
    }

    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        if (!$this->config->getConfig()['enable']) {
            return $this->_redirect('noroute');
        }

        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
