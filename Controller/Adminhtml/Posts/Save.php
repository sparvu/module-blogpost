<?php

namespace Sorin\Blogpost\Controller\Adminhtml\Posts;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Sorin\Blogpost\Api\Data\PostInterface;
use Sorin\Blogpost\Api\PostRepositoryInterface;
use Sorin\Blogpost\Exception\InvalidAPIResponse;
use Sorin\Blogpost\Model\PostFactory;
use Magento\Framework\App\CsrfAwareActionInterface;

class Save extends Action implements HttpPostActionInterface, CsrfAwareActionInterface
{
    /**
     * @var PostRepositoryInterface
     */
    private PostRepositoryInterface $postRepository;

    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param PostRepositoryInterface $postRepository
     * @param PostFactory $postFactory
     */
    public function __construct(
        Context $context,
        PostRepositoryInterface $postRepository,
        PostFactory $postFactory
    ) {
        parent::__construct($context);

        $this->postRepository = $postRepository;
        $this->postFactory = $postFactory;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $postId = $this->getRequest()->getParam('id');
        $postData = $this->getRequest()->getPostValue();

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if ($postData) {
            try {
                /** @var PostInterface $post */
                $post = $this->postFactory->create();
                $post->setTitle($postData['title']);
                $post->setContent($postData['content']);
                $this->postRepository->save($post);
                $this->messageManager->addSuccessMessage(__('Successfully added post.'));
            } catch (InvalidAPIResponse $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        }

        $resultRedirect->setPath('blog/posts/');
        return $resultRedirect;
    }

    /**
     * @param RequestInterface $request
     * @return InvalidRequestException|null
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @param RequestInterface $request
     * @return bool|null
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
