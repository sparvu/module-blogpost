<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Sorin\Blogpost\Controller\Adminhtml\Posts;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Sorin\Blogpost\Api\PostRepositoryInterface;
use Sorin\Blogpost\Exception\InvalidAPIResponse;

class Delete extends Action implements HttpGetActionInterface
{
    const MENU_ID = 'Sorin_Blogpost::blog_posts';

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param PostRepositoryInterface $postRepository
     */
    public function __construct(
        Context $context,
        PostRepositoryInterface $postRepository
    ) {
        parent::__construct($context);

        $this->postRepository = $postRepository;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $postId = $this->getRequest()->getParam('id');
        if ($postId) {
            try {
                $this->postRepository->deleteById($postId);
                $this->messageManager->addSuccessMessage(__(sprintf("Post #%s has been deleted", $postId)));
                $this->_redirect('blog/posts/*');
            } catch (InvalidAPIResponse $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Something went wrong while deleting the post.'));
            }
        }

        $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl($this->getUrl('*')));
    }
}
