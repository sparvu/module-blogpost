Sorin\Blogpost module allows all users to see the latest topics posted on the /blog/posts page.
Users are allowed to add new posts without having to register. After installing the module, please enable the module 
from the admin configurations (Stores > Configurations > Blog > Posts > General)

# installing the module
- composer require sparvu/module-blogpost:1.0.0
- bin/magento module:enable Sorin_Blogpost
- bin/magento setup:upgrade

# configure
- Get your auth token from GoRest API
- Fill admin configuration (Stores > Configuration > Blog > Posts) 
    - API Endpoint e.g: https://gorest.co.in/public/v2
- Enable/disable posts
- Enable/disable comments

#### More test coverage planned in near future due to time constraints in the schedule.
- Lines: 17.03% 
- Functions and Methods: 33.33%
- Classes and Traits: 25.00%
- phpcs - clear
