<?php

namespace Sorin\Blogpost\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Data\Collection;
use Magento\Framework\Exception\NoSuchEntityException;
use Sorin\Blogpost\Api\Data;
use Sorin\Blogpost\Api\Data\PostInterface;
use Sorin\Blogpost\Api\PostRepositoryInterface;
use Sorin\Blogpost\Exception\InvalidAPIResponse;
use Sorin\Blogpost\Service\RestApi;
use Sorin\Blogpost\Api\Data\PostSearchResultsInterfaceFactory;
use Sorin\Blogpost\Model\PostFactory;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Framework\Data\CollectionFactory;

/**
 * Class PostRepository
 *
 * Using GoRest API result instead of linking this repository to Resource Model
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class PostRepository implements PostRepositoryInterface
{
    /**
     * @var RestApi
     */
    private RestApi $apiService;

    /**
     * @var PostFactory
     */
    private PostFactory $postFactory;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var PostSearchResultsInterfaceFactory
     */
    private PostSearchResultsInterfaceFactory $searchResultFactory;

    /**
     * PostRepository constructor.
     * @param RestApi $apiService
     * @param \Sorin\Blogpost\Model\PostFactory $postFactory
     * @param CollectionFactory $collectionFactory
     * @param PostSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        RestApi $apiService,
        \Sorin\Blogpost\Model\PostFactory $postFactory,
        CollectionFactory $collectionFactory,
        PostSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->apiService = $apiService;
        $this->postFactory = $postFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * @param int|null $entityId
     * @return PostInterface|null
     * @throws NoSuchEntityException
     */
    public function getByEntityId(?int $entityId): ?PostInterface
    {
        try {
            $response = $this->apiService->execute('posts/' . $entityId);

            /** @var PostInterface $post */
            $post = $this->postFactory->create();

            $post->setEntityId($entityId);
            $post->setTitle($response['title']);
            $post->setContent($response['body']);

            return $post;
        } catch (InvalidAPIResponse $e) {
            throw new NoSuchEntityException(__('Could not find post with ID #' . $entityId));
        }
    }

    /**
     * @param PostInterface $post
     * @return PostInterface
     * @throws CouldNotSaveException
     */
    public function save(PostInterface $post): PostInterface
    {
        try {
            if ($post->getEntityId()) {
                // if entity_id is set -> update
                $params = [
                    'form_params' => [
                        'title' => $post->getTitle(),
                        'body' => $post->getContent()
                    ]
                ];

                $this->apiService->execute(
                    'posts/' . $post->getEntityId(),
                    $params,
                    Request::METHOD_PATCH
                );
            } else {
                // if no entity_id -> create new post
                //TODO generate a random user_id to create a new post since users are not part of this release version
                $params = [
                    'form_params' => [
                        'title' => $post->getTitle(),
                        'body' => $post->getContent(),
                        'user_id' => random_int(1, 1000)
                    ]
                ];

                $response = $this->apiService->execute('posts', $params, Request::HTTP_METHOD_POST);
                $post->setEntityId($response['id']);
            }

            return $post;
        } catch (InvalidAPIResponse $e) {
            throw new CouldNotSaveException(__('Post could not be saved.'));
        }
    }

    /**
     * @param PostInterface $post
     * @throws CouldNotSaveException
     */
    public function delete(PostInterface $post)
    {
        try {
            $this->apiService->execute('posts/' . $post->getEntityId(), [], Request::HTTP_METHOD_DELETE);
        } catch (InvalidAPIResponse $e) {
            throw new CouldNotSaveException(__('Post could not be deleted.'));
        }
    }

    /**
     * @param $entityId
     * @return void
     * @throws CouldNotSaveException
     */
    public function deleteById($entityId): void
    {
        try {
            $this->apiService->execute('posts/' . $entityId, [], Request::HTTP_METHOD_DELETE);
        } catch (InvalidAPIResponse $e) {
            throw new CouldNotDeleteException(__('Post could not be deleted.'));
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return Data\PostSearchResultsInterface
     * @throws InvalidAPIResponse
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): Data\PostSearchResultsInterface
    {
        $collection = $this->collectionFactory->create();

        // retrieve api records
        $apiPosts = $this->apiService->execute('posts/');

        // adding API records to a empty collection
        foreach ($apiPosts as $apiPost) {
            $varienObject = new DataObject();
            $varienObject->setData($apiPost);
            $collection->addItem($varienObject);
        }

        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }

    /**
     * @return Collection
     * @throws InvalidAPIResponse
     */
    public function getCollection()
    {
        $collection = $this->collectionFactory->create();

        // retrieve api records
        $apiPosts = $this->apiService->execute('posts/');

        // adding API records to a empty collection
        foreach ($apiPosts as $apiPost) {
            $varienObject = new DataObject();
            $varienObject->setData($apiPost);
            $collection->addItem($varienObject);
        }

        $collection->load();

        return $collection;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[] = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     */
    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() == SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     */
    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return mixed
     */
    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}
