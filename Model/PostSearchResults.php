<?php

namespace Sorin\Blogpost\Model;

use Magento\Framework\Api\SearchResults;
use Sorin\Blogpost\Api\Data\PostSearchResultsInterface;

class PostSearchResults extends SearchResults implements PostSearchResultsInterface
{

}
