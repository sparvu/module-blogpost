<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Model;

/**
 * Class Registry
 * Registry class used to pass data
 * eg. Passing URL param to viewModel or UiComponent
 */
class Registry
{
    /** @var int|null */
    private ?int $postId = null;

    /**
     * @return int|null
     */
    public function getPostId(): ?int
    {
        return $this->postId;
    }

    /**
     * @param int $postId
     * @return void
     */
    public function setPostId(int $postId): void
    {
        $this->postId = $postId;
    }
}
