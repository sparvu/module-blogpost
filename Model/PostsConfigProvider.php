<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Configuration provider for Blog posts rendering on "blog/posts" page.
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class PostsConfigProvider implements ConfigProviderInterface
{
    const XPATH_CONFIG_BLOGPOST_ENABLE = 'blogpost/general/enable';
    const XPATH_CONFIG_BLOGPOST_API_ENDPOINT = 'blogpost/api/endpoint';
    const XPATH_CONFIG_BLOGPOST_API_TOKEN = 'blogpost/api/token';

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfiguration;

    /**
     * PostsConfigProvider constructor.
     *
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        $this->scopeConfiguration = $context->getScopeConfig();
    }

    /**
     * @inheritdoc
     */
    public function getConfig(): array
    {
        $configuration = [];
        $configuration['enable'] = $this->isEnabled();
        $configuration['api_endpoint'] = $this->getApiEndpoint();
        $configuration['token'] = $this->getApiAuthToken();
        $configuration['storeCode'] = 'default';

        return $configuration;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->scopeConfiguration->isSetFlag(
            static::XPATH_CONFIG_BLOGPOST_ENABLE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return null|string
     */
    public function getApiEndpoint(): ?string
    {
        return $this->scopeConfiguration->getValue(
            static::XPATH_CONFIG_BLOGPOST_API_ENDPOINT,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return null|string
     */
    public function getApiAuthToken(): ?string
    {
        return $this->scopeConfiguration->getValue(
            static::XPATH_CONFIG_BLOGPOST_API_TOKEN,
            ScopeInterface::SCOPE_STORE
        );
    }
}
