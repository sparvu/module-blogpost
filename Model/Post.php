<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Sorin\Blogpost\Api\Data\PostInterface;

class Post extends AbstractModel implements PostInterface, IdentityInterface
{
    /**
     * Blog post cache tag.
     */
    const CACHE_TAG = 'blog_post';

    /**
     * @return int|null
     */
    public function getEntityId(): ?int
    {
        return $this->getData(static::ENTITY_ID);
    }

    /**
     * @param int $entityId
     * @return Post
     */
    public function setEntityId($entityId): Post
    {
        return $this->setData(static::ENTITY_ID, $entityId);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->getData(static::TITLE);
    }

    /**
     * @param $title
     * @return Post
     */
    public function setTitle($title): Post
    {
        return $this->setData(static::TITLE, $title);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->getData(static::CONTENT);
    }

    /**
     * @param $content
     * @return Post
     */
    public function setContent($content): Post
    {
        return $this->setData(static::CONTENT, $content);
    }

    /**
     * @return string[]
     */
    public function getIdentities()
    {
        return [static::CACHE_TAG . '_' . $this->getEntityId()];
    }
}
