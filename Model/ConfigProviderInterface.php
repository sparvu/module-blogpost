<?php
/**
 * Copyright © Sorin_Blogpost. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Sorin\Blogpost\Model;

/**
 * Interface ConfigProviderInterface
 * @api
 */
interface ConfigProviderInterface
{
    /**
     * Retrieve assoc array of blog posts configuration
     *
     * @return array
     */
    public function getConfig();
}
